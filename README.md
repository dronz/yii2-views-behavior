Yii2 views behavior
===================
Yii2 views behavior

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dronz/yii2-views-behavior "*"
```

or add

```
"dronz/yii2-views-behavior": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \dronz\views\AutoloadExample::widget(); ?>```