<?php

	use dronz\views\models\View;
	use yii\db\Migration;

	/**
	 * Handles the creation of table `views`.
	 */
	class m181006_110203_create_views_table extends Migration{

		public $table = 'views';

		/**
		 * {@inheritdoc}
		 */
		public function safeUp(){
			$this->createTable($this->table, [
				'id'       => $this->primaryKey(),
				'date'     => $this->date()->notNull(),
				'model'    => "ENUM('none') NOT NULL",
				'model_id' => $this->integer()->notNull(),
				'user_id'  => $this->integer(),
				'views'    => $this->tinyInteger()->notNull()->unsigned(),
			]);

			$this->createIndex('views', $this->table, 'views');
			$this->createIndex('unique', $this->table, ['date', 'user_id', 'model', 'model_id'], TRUE);
			View::rebuildEnum();
		}

		/**
		 * {@inheritdoc}
		 */
		public function safeDown(){
			$this->dropTable($this->table);
		}
	}
