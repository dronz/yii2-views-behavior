<?php

	use yii\db\Migration;

	/**
	 * Class m181015_084233_alter_user_id_column_in_views_table
	 */
	class m181015_084233_alter_user_id_column_in_views_table extends Migration{

		public $table = 'views';

		/**
		 * {@inheritdoc}
		 */
		public function safeUp(){

			$this->delete($this->table, ['user_id' => NULL]);
			$this->alterColumn($this->table, 'user_id', $this->integer()->notNull());
		}

		/**
		 * {@inheritdoc}
		 */
		public function safeDown(){
			$this->alterColumn($this->table, 'user_id', $this->integer());
		}

	}
