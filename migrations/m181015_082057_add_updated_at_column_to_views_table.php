<?php

	use yii\db\Expression;
	use yii\db\Migration;

	/**
	 * Handles adding updated_at to table `views`.
	 */
	class m181015_082057_add_updated_at_column_to_views_table extends Migration{

		public $table = 'views';

		/**
		 * {@inheritdoc}
		 */
		public function safeUp(){
			$this->addColumn($this->table, 'updated_at', $this->integer()->unsigned()->after('date'));
			$this->createIndex('updated_at', $this->table, 'updated_at');
			$this->update($this->table, ['updated_at' => new Expression('UNIX_TIMESTAMP(date)')]);
		}

		/**
		 * {@inheritdoc}
		 */
		public function safeDown(){
			$this->dropColumn($this->table, 'updated_at');
		}
	}
