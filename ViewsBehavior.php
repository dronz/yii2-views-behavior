<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 6.10.18
	 * Time: 13.57
	 */

	namespace dronz\views;


	use dronz\views\models\View;
	use Yii;
	use yii\base\Behavior;
	use yii\db\ActiveRecord;
	use yii\db\Expression;
	use function get_class;

	/**
	 * @property ActiveRecord $owner
	 */
	class ViewsBehavior extends Behavior{

		const
			EVENT_VIEW = 'view';

		public function events(){
			return [
				self::EVENT_VIEW => 'addView'
			];
		}

		public function addView(){
			/**@var View $view
			 */
			$owner = $this->owner;
			$sql   = Yii::$app->db->createCommand()->insert(View::tableName(), [
				'date'       => new Expression('date(now())'),
				'updated_at' => new Expression('UNIX_TIMESTAMP()'),
				'model'      => get_class($owner),
				'model_id'   => $owner->primaryKey,
				'user_id'    => (int)Yii::$app->user->id,
				'views'      => 1
			])->rawSql;
			Yii::$app->db->createCommand($sql.' ON DUPLICATE KEY UPDATE views=views+1,updated_at=UNIX_TIMESTAMP()')->execute();
		}

		public function getViews($today = FALSE){
			$model = View::find()->model($this->owner);
			if($today){
				$model->today();
			}

			return $model->sum('views');
		}

	}