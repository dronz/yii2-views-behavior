<?php

	namespace dronz\views\models;

	use Yii;
	use yii\db\ActiveRecord;
	use yii\db\Expression;

	/**
	 * This is the ActiveQuery class for [[View]].
	 * @see View
	 */
	class ViewsQuery extends \yii\db\ActiveQuery{

		public function model(ActiveRecord $model){
			return $this->andWhere(['model' => get_class($model), 'model_id' => $model->primaryKey]);
		}

		public function today(){
			return $this->andWhere(['date' => new Expression('date(NOW())')]);
		}

		public function sort(){
			return $this->orderBy(['updated_at' => SORT_DESC]);
		}

		public function my(){
			return $this->andWhere(['user_id' => (int)Yii::$app->user->id]);
		}

		/**
		 * {@inheritdoc}
		 * @return View[]|array
		 */
		public function all($db = NULL){
			return parent::all($db);
		}

		/**
		 * {@inheritdoc}
		 * @return View|array|null
		 */
		public function one($db = NULL){
			return parent::one($db);
		}
	}
