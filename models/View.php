<?php

	namespace dronz\views\models;

	use Yii;
	use yii\behaviors\TimestampBehavior;
	use yii\db\BaseActiveRecord;

	/**
	 * This is the model class for table "views".
	 * @property int                                $id
	 * @property string                             $date
	 * @property string                             $model
	 * @property int                                $model_id
	 * @property int                                $user_id
	 * @property int                                $views
	 * @property app\models\User|common\models\User $user
	 */
	class View extends \yii\db\ActiveRecord{

		/**
		 * {@inheritdoc}
		 */
		public static function tableName(){
			return 'views';
		}

		public function behaviors(){
			return [
				'timestamp' => [
					'class'      => TimestampBehavior::class,
					'attributes' => [
						BaseActiveRecord::EVENT_BEFORE_INSERT => $this->updatedAtAttribute,
						BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedAtAttribute,
					]
				]
			];
		}

		/**
		 * {@inheritdoc}
		 */
		public function rules(){
			return [
				[['date', 'model', 'model_id', 'views'], 'required'],
				[['date'], 'safe'],
				['updated_at', 'integer'],
				[['model_id', 'user_id', 'views'], 'integer'],
				[['model'], 'string', 'max' => 255],
				[['date', 'user_id', 'model', 'model_id'], 'unique', 'targetAttribute' => ['date', 'user_id', 'model', 'model_id']],
				[['user_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['user_id' => 'id']],
			];
		}

		/**
		 * {@inheritdoc}
		 */
		public function attributeLabels(){
			return [
				'id'         => 'ID',
				'date'       => 'Date',
				'updated_at' => 'Updated At',
				'model'      => 'Model',
				'model_id'   => 'Model ID',
				'user_id'    => 'User ID',
				'views'      => 'Views',
			];
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getUser(){
			return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'user_id']);
		}

		/**
		 * {@inheritdoc}
		 * @return ViewsQuery the active query used by this AR class.
		 */
		public static function find(){
			return new ViewsQuery(get_called_class());
		}

		public static function rebuildEnum(){
			$models = [];
			foreach(Yii::$app->params['viewModels'] as $model){
				$models[] = Yii::$app->db->quoteValue($model);
			}
			Yii::$app->db->createCommand()->alterColumn(self::tableName(), 'model', "ENUM(".implode(',', $models).") NOT NULL")->execute();
		}
	}
